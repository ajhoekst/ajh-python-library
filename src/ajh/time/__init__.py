import calendar
import datetime

def end_of_month(date):
    return datetime.date(
        year=date.year,
        month=date.month,
        day=calendar.monthrange(
            year=date.year,
            month=date.month,
        )[1]
    )
